import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/**
 * Created by user on 26/09/16.
 */
public class PercolationStats {

    private double mean;
    private double stddev;
    private double[] results;

    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) throw new IllegalArgumentException();

        results = new double[trials];

        Percolation[] percolations = new Percolation[trials];
        for (int i = 0; i < trials; i++) {
            percolations[i] = new Percolation(n);
        }

        double[] results = new double[trials];
        for (int i = 0; i < trials; i++) {
            results[i] = trial(percolations[i], n);
        }

        mean = StdStats.mean(results);
        stddev = StdStats.stddev(results);
    }
    private double trial(Percolation percolation, int n) {
        for (int c = 1; c <= n * n; c++) {
            int i = StdRandom.uniform(n + 1);
            int j = StdRandom.uniform(n + 1);
            while ((i == 0 || j == 0) || percolation.isOpen(i, j)) {
                i = StdRandom.uniform(n + 1);
                j = StdRandom.uniform(n + 1);
            }
            percolation.open(i, j);
            if (percolation.percolates()) {
                return c / (double) (n * n);
            }
        }
        return 0;
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stddev;
    }

    public double confidenceLo() {
        return mean - 1.96*stddev/Math.sqrt(results.length);
    }

    public double confidenceHi() {
        return mean + 1.96*stddev/Math.sqrt(results.length);
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException();
        }

        int size = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);

        if (size <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }

        PercolationStats percolationStats = new PercolationStats(size, trials);

        System.out.println("mean = " + percolationStats.mean());
        System.out.println("stddev = " + percolationStats.stddev());
        System.out.println("95% confidence interval = " + percolationStats.confidenceLo() + ", " + percolationStats.confidenceHi());

    }
}
