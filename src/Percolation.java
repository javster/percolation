import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * Created by javster on 25/09/16.
 */
public class Percolation {

    private static final int OPEN = 0x1;
    private static final int ENTRY_SHIFT = 1;

    private final int n;
    private int[][] matrix;
    private WeightedQuickUnionUF uf;

    public Percolation(int n) {
        if (n <= 0) throw new IllegalArgumentException();
        this.n = n;
        uf = new WeightedQuickUnionUF(n * (n + 1) + 2);
        matrix = new int[n][n];
    }

    private void checkIndices(int i, int j) {
        if (i <= 0 || j <= 0 || i > n || j > n) {
            throw new IndexOutOfBoundsException();
        }
    }

    private int toIndex(int i, int j) {
        return (i - 1) * n + (j - 1) + ENTRY_SHIFT;
    }

    public void open(int i, int j) {
        checkIndices(i, j);
        int index = toIndex(i, j);
        if (j > 1) {
            if (isOpen(i, j - 1)) {
                uf.union(index, toIndex(i, j - 1));
            }
        }
        if (j < n) {
            if (isOpen(i, j + 1)) {
                uf.union(index, toIndex(i, j + 1));
            }
        }
        if (i > 1) {
            if (isOpen(i - 1, j)) {
                uf.union(index, toIndex(i - 1, j));
            }
        } else {
            uf.union(index, 0);
        }
        if (i < n) {
            if (isOpen(i + 1, j)) {
                uf.union(index, toIndex(i + 1, j));
            }

            if (uf.connected(index, 0)) {
                if (!uf.connected(index, getEndIndex())) {
                    if (isFakeConnected(index)) {
                        uf.union(index, getEndIndex());
                    }
                }
            }

        } else {
            if (uf.connected(index, 0)) {
                // connect with real end
                uf.union(index, getEndIndex());
            } else {
                // connect with fake end
                uf.union(index, n * n + j);
            }
        }
        matrix[i - 1][j - 1] = OPEN;
    }

    private boolean isFakeConnected(int d) {
        for (int i = n * n; i < (n + 1) * n; i++) {
            if (uf.connected(d, i)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOpen(int i, int j) {
        checkIndices(i, j);
        return matrix[i - 1][j - 1] == OPEN;
    }

    public boolean isFull(int i, int j) {
        checkIndices(i, j);
        boolean connected = uf.connected(0, toIndex(i, j));

        return connected;
    }

    public boolean percolates() {
        return uf.connected(0, getEndIndex());
    }

    private int getEndIndex() {
        return n * (n + 1) + 1;
    }

    public static void main(String[] args) {
    }
}